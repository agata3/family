import React from 'react'
import Common from '../common'
//import { Link } from 'react-router-dom'
import zdjecie from '../assets/aga.jpeg'
import zdjecie1 from '../assets/jaflipo.jpeg'
import Typography from '@material-ui/core/Typography'

const Ja = () => {
  return (
    <div>
      <Typography variant="h4" gutterBottom>
        Ja
      </Typography>
      <Typography>
        Czasem użalam się nad swym losem, Ale wiem,że niektórzy mają gorzej.
        Jednak swoje już przeżyłam, dlatego ostatnio się zmieniłam.
      </Typography>
      <Common href="https://www.facebook.com/agata.szczypta.5" />
      <img src={zdjecie} width={160} alt="aga" />
      <img src={zdjecie1} width={160} alt="japlipo" />
    </div>
  )
}

export default Ja
