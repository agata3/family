import React from 'react'
import zd from '../assets/flip.jpeg'
import zd1 from '../assets/flipi.jpeg'
import zd2 from '../assets/korcia4.jpg'
import zd3 from '../assets/korciacar.jpg'
import zd4 from '../assets/korcialala.jpg'
import Typography from '@material-ui/core/Typography'
import { Grid } from '@material-ui/core'

const korciaflipi = () => {
  return (
    <div>
      <Typography variant="h4" gutterBottom>
        Korcia
      </Typography>

      <Typography>
        <p>
          Dziecko może nauczyć dorosłych trzech rzeczy: cieszyć się bez powodu,
          być ciągle czymś zajętym i domagać się - ze wszystkich sił - tego,
          czego pragnie."
          <br />
          Uśmiech dziec­ka to naj­wspa­nial­sze zja­wis­ko ja­kie mógł człowiek
          zo­baczyć na swo­je oczy .
        </p>
        <Grid container spacing={1}>
          <Grid item xs={12} sm={2} md={4}>
            <div
              style={{
                backgroundImage: `url(${zd2})`,
                backgroundSize: 'cover',
                height: 300,
              }}
            />
          </Grid>
          <Grid item xs={12} sm={2} md={4}>
            <div
              style={{
                backgroundImage: `url(${zd3})`,
                backgroundSize: 'cover',
                height: 300,
              }}
            />
          </Grid>
        </Grid>
        <Typography variant="h4" gutterBottom>
          Flipi
        </Typography>

        <p>
          {' '}
          Gdyby psy potrafiły mówić, posiadanie ich nie byłoby już tak
          przyjemne.
          <br />
          Piesek tak pilnuje Pana, bo wie dobrze, już od lat, że Pan to jego
          cały świat…
        </p>
        <img src={zd} width={160} alt="flip" />
        <img src={zd1} width={160} alt="flipi" />
        <img src={zd2} width={160} alt="korcia4" />
        <img src={zd3} width={300} alt="korciacar" />
        <img src={zd4} width={300} alt="korcialala" />
      </Typography>
    </div>
  )
}

export default korciaflipi
