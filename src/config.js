import { createMuiTheme } from '@material-ui/core/styles'

export const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#5216ac'
        },
        secondary: {
            main: '#bf360c'
        }
    },
    typography: {
        fontFamily: 'Comfortaa'
    }
})
