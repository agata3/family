import React from 'react'
// import './style.css'
import ticknock from './assets/ticknock.jpeg'

const Topbar = () => {
  return (
    <div id="topbar">
      <div id="topbarL">
        <img src={ticknock} width={160} alt="Dublin" />
      </div>
      <div id="topbarR">
        <div id="bigtitle1">O projekcie slow kilka</div>Moj pierwszy projekt o
        mojej rodzinie i o mnie:)
        <div className="dottedline" />
      </div>
      <div style={{ clear: 'both' }} />
    </div>
  )
}

export default Topbar
