import React from 'react'
import ReactDOM from 'react-dom'
// import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { BrowserRouter as Router } from 'react-router-dom'
import CssBaseline from '@material-ui/core/CssBaseline'
import { MuiThemeProvider } from '@material-ui/core/styles'
import { theme } from './config'
ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Router>
      <React.Fragment>
        <CssBaseline />
        <App />
      </React.Fragment>
    </Router>
  </MuiThemeProvider>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
