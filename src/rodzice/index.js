import React from 'react'

import zd from './mam.jpg'
import zd1 from './wakacje.jpeg'
import zd2 from './wakacje1.jpeg'
import zd3 from '../assets/mamtat.jpeg'
import Typography from '@material-ui/core/Typography'
import { Grid } from '@material-ui/core'

const Rodzice = () => {
  return (
    <div>
      <Typography variant="h4" gutterBottom>
        Rodzice
      </Typography>

      <Typography>
        <br />
        <br />
        Dziękuję Bogu, że Was mam.
        <br />
        Za wszystkie lata, które mi dali,
        <br />
        <br />
        Czasem ganili lecz zawsze wspierali,
        <br /> Mówili jak w życiu mam postępować,
        <br />
        <br />
        Chcąc jak najlepiej córkę wychować.
        <br />
        Rodzice mnie nauczyli, miłość
        <br />
        <br />
        nadzieję w serce wpoili.
        <br /> Bardzo dziękuję mamo i tato Za każdą zimę i każde lato,
        <br />
        <br />
        Za wiosny zapach,barwę jesieni,
        <br /> Nic mej miłości nigdy nie zmieni.
        <br />
        Mam nadzieję, że Bóg pozwoli mi cieszyć się Wami jak najdłużej. Kocham
        Was bardzo.
      </Typography>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={6} md={3}>
          <div
            style={{
              backgroundImage: `url(${zd})`,
              backgroundSize: 'cover',
              height: 360,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <div
            style={{
              backgroundImage: `url(${zd1})`,
              backgroundSize: 'cover',
              height: 360,
              backgroundPositionY: -90,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <div
            style={{
              backgroundImage: `url(${zd2})`,
              backgroundSize: 'cover',
              height: 360,
              backgroundPositionY: -90,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <div
            style={{
              backgroundImage: `url(${zd3})`,
              backgroundPositionX: -150,

              backgroundSize: 'cover',
              height: 360,
            }}
          />
        </Grid>
      </Grid>
    </div>
  )
}

export default Rodzice
