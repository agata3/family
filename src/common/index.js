import React from 'react'
import Fab from '@material-ui/core/Fab'
import FacebookBox from 'mdi-material-ui/FacebookBox'

const common = props => {
    return (
        <div style={{ marginTop: 30, marginBottom: 15 }}>
            <Fab
                href={props.href}
                variant='extended'
                color={props.color || 'primary'}
                {...props}
            >
                {props.icon || <FacebookBox style={{ marginRight: 4 }} />}
                {props.imie} {props.nazwisko}
            </Fab>
        </div>
    )
}

export default common
