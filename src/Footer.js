import React from 'react'
import Typography from '@material-ui/core/Typography'
import Toolbar from '@material-ui/core/Toolbar'

const Footer = () => {
  return (
    <Toolbar variant="dense">
      <Typography>
        Poznaliscie Mnie i Moja wspaniala rodzine &copy;2019
      </Typography>
    </Toolbar>
  )
}

export default Footer
