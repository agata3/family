import React from 'react'
import podworze from './assets/podworze.jpg'
import zd from './assets/shopping.jpeg'
import zd1 from './assets/dublinclif.jpg'
//import Fab from '@material-ui/core/Fab'
import { Button, Typography, Grid } from '@material-ui/core'

const Home = () => {
  return (
    <div>
      <div style={{ marginBottom: 20 }} id="bigtitle">
        <Button
          target="_blank"
          href="https://www.youtube.com/watch?v=r3btlOJhJmc"
          variant="outlined"
          color="primary"
        >
          DUBLIN
        </Button>
        <Typography>
          <br />
          <i>Dom jest tam, gdzie chcą, żebyś został dłużej.</i> Moim domem stal
          sie Dublin.
          <br />
          Irlandia
          <br />
          <br />
          Zielone góry
          <br />
          kwadraty pól
          <br />
          przydrożne mury
          <br />
          kaprysy chmur
          <br />
          <br />
          palmy w ogrodzie
          <br />
          uśmiech sąsiada
          <br />
          tu każdy z każdym
          <br />
          chętnie pogada
          <br />
          <br />
          i czy go znasz
          <br />
          czy też nie
          <br />
          przechodząc obok
          <br />
          uśmiechnie się
          <br />
        </Typography>
      </div>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={6} md={4}>
          <div
            style={{
              backgroundImage: `url(${zd})`,
              backgroundSize: 'cover',
              height: 200,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <div
            style={{
              backgroundImage: `url(${zd1})`,
              backgroundSize: 'cover',
              height: 200,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <div
            style={{
              backgroundImage: `url(${podworze})`,
              backgroundSize: 'cover',
              height: 200,
            }}
          />
        </Grid>
      </Grid>
    </div>
  )
}

export default Home
