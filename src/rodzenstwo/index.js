import React from 'react'
import zd from '../assets/rodzenstwo.jpeg'
import zd1 from '../assets/piotrek.jpeg'
import zd2 from '../assets/lukaszkasia.jpeg'
import zd3 from '../assets/jaemikas.jpeg'
import zd4 from '../assets/jaemi.jpeg'
import Button from '../common'
import Typography from '@material-ui/core/Typography'
import { Grid } from '@material-ui/core'

const rodzenstwo = () => {
  return (
    <div>
      <Typography variant="h4" gutterBottom>
        Rodzenstwo
      </Typography>

      <Typography style={{ marginBottom: 16 }}>
        Rodzenstwo- chocby nie wiem co sie stalo i ile dzielilo je kilometrow,
        <br />
        zawsze nawzajem sie wspiera i wskoczyloby za soba w ogien. <br />
        Tak jest ze mna i moim kochanym rodzenstwem...
        <br /> Kocham Was Kasiu, Piotrku i najmlodsza Emilko.{' '}
      </Typography>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={6} md={6}>
          <div
            style={{
              backgroundImage: `url(${zd})`,
              backgroundSize: 'cover',
              height: 250,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <div
            style={{
              backgroundImage: `url(${zd2})`,
              backgroundSize: 'cover',
              height: 250,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <div
            style={{
              backgroundImage: `url(${zd1})`,
              backgroundSize: 'cover',
              height: 450,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <div
            style={{
              backgroundImage: `url(${zd4})`,
              backgroundSize: 'cover',
              height: 450,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <div
            style={{
              backgroundImage: `url(${zd3})`,
              backgroundSize: 'cover',
              backgroundPositionX: -40,

              height: 300,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <div style={{ textAlign: 'center' }}>
            <Button
              size="small"
              href="https://www.facebook.com/katarzyna.szczypta.3"
              imie="Kasia"
            />
            <Button
              size="small"
              href="https://www.facebook.com/piotrek.szczypta.3"
              imie="Piotrek"
              // color="secondary"
            />
            <Button
              size="small"
              href="https://www.facebook.com/emilia.szczypta"
              imie="Emilka"
            />
          </div>
        </Grid>{' '}
      </Grid>
    </div>
  )
}

export default rodzenstwo
