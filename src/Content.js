import React from 'react'
import Paper from '@material-ui/core/Paper'

const Content = props => {
  return <Paper>{props.children}</Paper>
}

export default Content
