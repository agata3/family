import React from 'react'
import { Route } from 'react-router-dom'

import Home from './Home'
import Rodzice from './rodzice'
import RodzinkaDublin from './dublin'
import Rodzenstwo from './rodzenstwo'
import KorciaFlipi from './korciaflipi'
import Ja from './ja'

const PageRouter = () => {
  return (
    <div>
      <Route exact path="/" component={Home} />
      <Route path="/rodzice" component={Rodzice} />
      <Route path="/dublin" component={RodzinkaDublin} />
      <Route path="/rodzenstwo" component={Rodzenstwo} />
      <Route path="/korciaflipi" component={KorciaFlipi} />
      <Route path="/ja" component={Ja} />
    </div>
  )
}

export default PageRouter
