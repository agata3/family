import React from 'react'
import zd from '../assets/agataeryk.jpg'
import zd1 from '../assets/erykgabi.jpeg'
import zd2 from '../assets/erykmaly.jpeg'
import zd3 from '../assets/rodzinkapark.jpeg'
import zd4 from '../assets/youtube-icon.png'
import Common from '../common'
import { Button, Grid } from '@material-ui/core'

import Typography from '@material-ui/core/Typography'

const Dublin = () => {
  return (
    <div>
      <Typography color="primary" variant="h4" gutterBottom>
        Rodzina w Dublinie
      </Typography>
      <Grid container style={{ marginBottom: 20 }}>
        <Grid item xs={12} sm={4} style={{ padding: 15 }}>
          <Typography
            className="colorred"
            // style={{ float: 'left', marginRight: 30, marginBottom: 30 }}
          >
            Rodzina to nie przeszłość,
            <br /> nie można o niej nie myśleć. <br />
            Człowiek ma ją w sercu,
            <br /> dokądkolwiek idzie.
          </Typography>
        </Grid>
        <Grid item xs={12} sm={4} style={{ padding: 15 }}>
          <Typography
          // style={{ float: 'left' }}
          >
            Eryk Każdą sekundę mojego życia nazywam Twoim imieniem,
            <br /> bez Ciebie moja dziecinko świat dla mnie nie istnieje.
          </Typography>
        </Grid>
        <Grid item xs={12} sm={4} style={{ padding: 15 }}>
          <Typography
          // style={{ float: 'left', marginLeft: 30 }}
          >
            Naj­piękniej­sze ob­licze świata <br />
            to te widziane ocza­mi uśmie­chnięte­go dziecka.{' '}
          </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={6} md={3}>
          <div
            style={{
              backgroundImage: `url(${zd})`,
              backgroundSize: 'cover',
              height: 200,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <div
            style={{
              backgroundImage: `url(${zd1})`,
              backgroundSize: 'cover',
              height: 200,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <div
            style={{
              backgroundImage: `url(${zd2})`,
              backgroundSize: 'cover',
              height: 200,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <div
            style={{
              backgroundImage: `url(${zd3})`,
              backgroundSize: 'cover',
              height: 200,
            }}
          />
        </Grid>
      </Grid>
      <div>
        <Common
          size="small"
          href="https://www.facebook.com/tkostus"
          imie="Tomek"
        />
        <Button
          size="small"
          target="_blank"
          href="https://www.youtube.com/channel/UCnKP5YEklItK7LIBQWxV9Aw"
          variant="outlined"
          color="secondary"
        >
          <img src={zd4} width={25} alt="youtube" />
          Eryk
        </Button>
      </div>
    </div>
  )
}
/* <div
        class="gridwrapper"
        style={{
          display: 'grid',
          gridTemplateColumns: 'repeat(5, 1fr)',
          gridTemplateRows: '1fr',
          width: '100%',
          height: 200,
          gridGap: '15px',
        }}
      >
        <div
          style={{
            height: '100%',
            width: '100%',
            backgroundImage: `url(${zd})`,
            backgroundSize: 'cover',
          }}
        />
        <div
          style={{
            height: '100%',
            width: '100%',
            backgroundImage: `url(${zd1})`,
            backgroundSize: 'cover',
          }}
        />
        <div
          style={{
            height: '100%',
            width: '100%',
            backgroundImage: `url(${zd2})`,
            backgroundSize: 'cover',
          }}
        />
        <div
          style={{
            height: '100%',
            width: '100%',
            backgroundImage: `url(${zd3})`,
            backgroundSize: 'cover',
          }}
        />

       
       
      </div> */

export default Dublin
