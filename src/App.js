import React, { Component } from 'react'

import Paper from '@material-ui/core/Paper'

// import './style.css'

import Nav from './Nav'
import Menu from './Menu'
import Router from './Router'
import Footer from './Footer'

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Nav />
        <Menu {...this.props} />
        {/* <Topbar /> */}
        <Paper style={{ margin: 24, padding: 12 }}>
          <Router />
        </Paper>
        <Footer />
      </React.Fragment>
    )
  }
}

export default App
