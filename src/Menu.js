import React from 'react'
// import './style.css'
import { withRouter } from 'react-router-dom'

import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import AppBar from '@material-ui/core/AppBar'

const Menu = props => {
  console.log(props.location.pathname)
  const location = props.location.pathname

  const menuItems = [
    { label: 'Home', location: '/' },
    { label: 'Rodzice', location: '/rodzice' },
    { label: 'Rodzinka Dublin', location: '/dublin' },
    { label: 'Rodzenstwo', location: '/rodzenstwo' },
    { label: 'Korcia Flipi', location: '/korciaflipi' },
    { label: 'Ja', location: '/ja' }
  ]

  const locationIndex = menuItems.findIndex(i => location === i.location)

  return (
    <AppBar position="static" elevation={0}>
      <Tabs scrollButtons="off" scrollable value={locationIndex}>
        {menuItems.map(i => (
          <Tab
            label={i.label}
            key={i.label}
            onClick={() => props.history.push(i.location)}
          />
        ))}
        {/* <Tab label="Home" onClick={() => props.history.push('/')} />
        <Tab label="Rodzice" onClick={() => props.history.push('/rodzice')} />

        <Tab
          label="Rodzinka Dublin"
          onClick={() => props.history.push('/dublin')}
        />

        <Tab
          label="Rodzenstwo"
          onClick={() => props.history.push('/rodzenstwo')}
        />
        <Tab
          label="Korcia Flipi"
          onClick={() => props.history.push('/korciaflipi')}
        />

        <Tab label="Ja" onClick={() => props.history.push('/ja')} /> */}
      </Tabs>
    </AppBar>
  )
}

export default withRouter(Menu)
